variable "role_name" {
  type        = string
  description = "Name of the role to create"
}

variable "role_description" {
  type        = string
  description = "Description of the role"
}

variable "role_tags" {
  type        = map(any)
  description = "Tags that should be applied to the IAM role"
  default     = {}
}

variable "k8s_namespace" {
  type        = string
  description = "Name of the kubernetes namespace that contains the service account that will be authorized to assume the role"
}

variable "k8s_service_account_name" {
  type        = string
  description = "Name of the service account that will be authorized to assume the role"
}

variable "create_oidc_provider" {
  type        = bool
  description = "Should the OpenId Provider be created?"
  default     = true
}

variable "cluster" {
  type        = string
  description = "Cluster that your app is running in"
  default     = "prod"
}

# This only exists to support ephemeral platform clusters and should
# not be used in normal operations. Simply use "cluster"
variable "cluster_endpoint" {
  type        = string
  description = "A specific cluster endpoint"
  default     = ""
}
