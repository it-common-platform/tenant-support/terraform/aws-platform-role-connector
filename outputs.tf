output "role" {
  value = {
    name = aws_iam_role.oidc.name
    id   = aws_iam_role.oidc.id
    arn  = aws_iam_role.oidc.arn
  }
  description = "Details about the created IAM role"
}

output "provider_arn" {
  value       = var.create_oidc_provider ? aws_iam_openid_connect_provider.oidc.0.arn : null
  description = "The arn of the OIDC Provider, if created. Otherwise, null."
}
