# platform-role-connector

## Purpose

This module provisions simplifies the process to connect a Platform Cluster to 
securely assume a role in another account, with the goal to allow teams to have the ability
to access resources in other AWS accounts.

## Description

To support the ability for applications in the Common Platform to access resources in their
own AWS accounts, we are leveraging the [EKS Pod Identity Webhook](https://github.com/aws/amazon-eks-pod-identity-webhook). Without getting into great details, it works by doing the following:

- Every service account in Kubernetes is given a JWT to identify itself, signed by a cluster key
- Other AWS accounts add an Identity Provider that points to the OIDC of the cluster
- Trust relationships are created in the recipient account to authorize a specific service account in
  a specific namespace to assume a specific role
- As the app starts, the AWS SDK will automatically assume the role

To use this module and configure your role, you will need to know both the **namespace** and the 
**service account name** that your application will be using while running on the platform. 

As an advanced use case, you are welcome to create as many service accounts within your namespace.
This would provide the ability to run multiple pods within a single namespace and grant each one access
to separate AWS roles.


## Usage Instructions

Copy and paste into your Terraform configuration, insert or update the
variables, and run `terraform init`:

```
module "platform_role_connector" {
  source                   = "git@code.vt.edu:it-common-platform/support/terraform/aws-platform-role-connector.git"
  role_name                = "cluster-app-role"
  role_description         = "A role that can be assumed by the Common Platform"
  k8s_namespace            = "it-common-platform-hello-world-app"
  k8s_service_account_name = "default"

  role_tags = {
    ResponseParty = "sally"
  }
}

# Use the role that was created in an IAM policy attachment
resource "aws_iam_role_policy_attachment" "example_attach" {
  role       = module.platform_role_connector.role.name
  policy_arn = "the-arn-to-connect"
}
```

### Usage Instructions, in Kubernetes

Once you have created your role, you can update your ServiceAccount and specify the role you wish to assume:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: hello-world-app
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::1234567890123:role/my-app-role
```

### Defining Multiple Roles

If you need to define multiple roles that should be assumed, only one of them should define the OIDC Provider. All others should set `create_oidc_provider` to `false`.

```hcl
module "cluster_role_one" {
  source                   = "git@code.vt.edu:it-common-platform/support/terraform/aws-platform-role-connector.git"
  role_name                = "cluster-app-role1"
  role_description         = "A role that can be assumed by the Common Platform"
  k8s_namespace            = "it-common-platform-hello-world-app"
  k8s_service_account_name = "default"
}

module "cluster_role_two" {
  source                   = "git@code.vt.edu:it-common-platform/support/terraform/aws-platform-role-connector.git"
  role_name                = "cluster-app-role2"
  role_description         = "A role that can be assumed by the Common Platform"
  k8s_namespace            = "it-common-platform-hello-world-app"
  k8s_service_account_name = "app"
  create_oidc_provider     = false
}

resource "aws_iam_role_policy_attachment" "example_attach_one" {
  role       = module.cluster_role_one.role.name
  policy_arn = "the-arn-to-connect-for-role-one"
}

resource "aws_iam_role_policy_attachment" "example_attach_two" {
  role       = module.cluster_role_two.role.name
  policy_arn = "the-arn-to-connect-for-role-two"
}
```

## Inputs

### Required Inputs
These variables are required.

| Name | Type | Description |
| ---- | ---- | ----------- |
| **role_name** | `[string]` | The name of the IAM role to create |
| **role_description** | `[string]` |  The description for the IAM role |
| **k8s_namespace** | `[string]` | The namespace that contains the service account that will be authorized to assume role |
| **k8s_service_account_name** | `[string]` | The name of the service account in the namespace that will be authorized to assume role

### Optional Inputs
These variables have default values and don't have to be set to use this module.
You may set these variables to override their default values.

| Name | Type | Description | Default |
| ---- | ---- | ----------- | ------- |
| **role_tags** | `map<string, string>` | Optional tags to apply to the IAM role | `{}`
| **create_oidc_provider** | `[bool]` | If true, an IAM Identity Provider will be created to point to the cluster's OIDC endpoint | `true`
| **cluster** | `[string]` | The specific cluster to leverage for the OIDC provider [`prod`,`pprd`,`dvlp`] | `prod`
| **cluster_endpoint** | `[string]` | A specific cluster endpoint | `""`


## Outputs

The following outputs are exported by the module. Each of the outputs is an object that provides additional details. As an example, if you defined the module using the example above, you could access the ARN of the created IAM role by using `module.platform-role-connector.role.arn`.

| Name | Description | Structure |
| ---- | ----------- | --------- |
| **role** | Details of the created IAM role | <ul><li><strong>name</strong> - the name of the role</li><li><strong>arn</strong> - the ARN of the role</li><li><strong>id</strong> - the ID of the role</ul>
| **provider_arn** | The ARN of the OIDC Identity Provider, if created | `[string]` if created, otherwise `null`


## Versions

| Version | Major changes |
| ------- | ------------- |
| 1     | First release |

