locals { 
  prod_endpoint = "oidc.eks.us-east-1.amazonaws.com/id/0488F38C8F97CB5B49EA6B41CE9BDA02"
  pprd_endpoint = "oidc.eks.us-east-1.amazonaws.com/id/A84B335B1C2AE8D86CE4CF759F603AEE"
  dvlp_endpoint = "oidc.eks.us-east-1.amazonaws.com/id/593F9C017C1F5E41C3AE06A1FA53E345"


  connector_url = "https://${local.url}"
  url           = (var.cluster_endpoint != "") ? var.cluster_endpoint : ((var.cluster == "prod") ? local.prod_endpoint : ((var.cluster == "pprd") ? local.pprd_endpoint: local.dvlp_endpoint))
  audience      = "sts.amazonaws.com"
}

data "aws_caller_identity" "current" {}
data "aws_partition" "current" {}

data "tls_certificate" "oidc_provider" {
  url = local.connector_url
}

resource "aws_iam_openid_connect_provider" "oidc" {
  count = var.create_oidc_provider ? 1 : 0

  url = local.connector_url

  client_id_list = [
    local.audience
  ]

  thumbprint_list = [
    data.tls_certificate.oidc_provider.certificates.0.sha1_fingerprint
  ]
}

data "aws_iam_policy_document" "oidc_trust_relationship" {
  statement {
    sid     = "AllowClusterAccess"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type = "Federated"
      identifiers = [
        "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.url}"
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "${local.url}:sub"
      values = [
        "system:serviceaccount:${var.k8s_namespace}:${var.k8s_service_account_name}"
      ]
    }
  }
}

resource "aws_iam_role" "oidc" {
  name               = var.role_name
  description        = var.role_description
  assume_role_policy = data.aws_iam_policy_document.oidc_trust_relationship.json
}
